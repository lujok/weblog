;; publish.el -- Publish Website using Org mode
;;
(require 'package)
(package-initialize)
(setq package-archives '(
			 ("gnu" . "https://elpa.gnu.org/packages/")
			 ("melpa" . "https://melpa.org/packages/")
			 ("nongnu" . "https://elpa.nongnu.org/nongnu/")
			 ))
(package-refresh-contents)
(package-install 'org-contrib)
(package-install 'htmlize)
(package-install 'dash)
(add-to-list 'load-path
	     (concat (file-name-directory (or load-file-name buffer-file-name)) "elisp"))
(require 'org)
(require 'ox-publish)
(require 'ox-rss)
(require 'dash)

(setq debug-on-error t) 
;; disable timestamps
;;(setq org-publish-use-timestamps-flag nil)
(setq user-full-name "桑下居")
(setq user-mail-address "luplus.zju@gmail.com")
;; Set default settings
(setq-default org-display-custom-times t)
(setq org-time-stamp-custom-formats 
  '("%Y-%m-%d" . "%Y-%m-%d %A %H:%M"))
(setq org-export-with-inlinetasks nil
      org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-statistics-cookies nil
      org-export-with-toc nil
      org-export-with-tasks nil
      org-export-html-style-include-default nil
      org-export-with-sub-superscripts nil
      org-export-html-head-include-default-style nil
      org-export-html-head-include-scripts nil
      org-export-date-timestamp-format "%Y-%m-%d")
;; HTML settings
(setq org-html-divs '((preamble "header" "top")
                      (content "main" "content")
                      (postamble "footer" "postamble"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-htmlize-output-type 'css
      org-html-doctype "html5")

(setq make-backup-files nil)
(setq org-src-fontify-natively t)
(setq org-rss-include-post-content t)
(setq org-confirm-babel-evaluate nil)
(setq postamble (with-temp-buffer
                  (insert-file-contents "templates/postamble.html")
                  (buffer-string))
      preamble (with-temp-buffer
                  (insert-file-contents "templates/preamble.html")
                  (buffer-string))
      header (with-temp-buffer
                  (insert-file-contents "templates/header.html")
		  (buffer-string))
      disqus (with-temp-buffer
                  (insert-file-contents "templates/disqus.html")
		  (buffer-string))
      valine (with-temp-buffer
                  (insert-file-contents "templates/valine.html")
		  (buffer-string))
      waline (with-temp-buffer
                  (insert-file-contents "templates/waline.html")
		  (buffer-string))
      twikoo (with-temp-buffer
                  (insert-file-contents "templates/twikoo.html")
		  (buffer-string))
      blogmeta (with-temp-buffer
                  (insert-file-contents "templates/blog.org")
		  (buffer-string))
      notemeta (with-temp-buffer
                  (insert-file-contents "templates/note.org")
		  (buffer-string))
      old-blogmeta (with-temp-buffer
                  (insert-file-contents "templates/old-blog.org")
               (buffer-string)))

(defun parent-dir (file)
  "Return the parent directory of FILE."
  (unless (equal "/" file)
    (file-name-directory (directory-file-name file))))
(setq website-title "桑下居"
      blog-title "桑下網誌"
      note-title "桑下書房"
      old-blog-title "此心安處"
      blog-root (parent-dir (or load-file-name buffer-file-name)))
(defvar website-url "https://luee.cn")
(defvar blog-url "https://luee.cn/blog")

(defun project-dir (&optional dir)
  "Get the absolute path of DIR as if it is a directory in BLOG-ROOT."
  (expand-file-name (or dir "") blog-root))

(defun root-link (link)
  "Append LINK to BLOG-ROOT."
  (concat (file-name-as-directory blog-url) link))

(defun lujok/blog-index (title list)
  (mapconcat
   'identity
   (list
    (concat "#+TITLE: " title "\n" blogmeta)
    (org-list-to-subtree list nil '(:istart "** ")))
   "\n"))

(defun lujok/note-index (title list)
  (mapconcat
   'identity
   (list
    ;(concat "#+TITLE: " title "\n" notemeta)
     (org-list-to-subtree list nil '(:istart "- ")))
   "\n"
   ))

(defun lujok/sitemap-index (title list)
  (mapconcat
   'identity
   (list
    (concat "#+TITLE: " title)
     (org-list-to-subtree list nil '(:istart "- ")))
   "\n"
   ))

(defun lujok/old-blog-index (title list)
  (mapconcat
   'identity
   (list
    (concat "#+TITLE: " title "\n" old-blogmeta)
    (org-list-to-subtree list nil '(:istart "** ")))
   "\n"))

(defun lujok/sitemap-format-entry (entry style project)
  (format "
    [[file:%s][%s]]
    #+HTML: <div class=\"description\">%s</div>
    #+HTML: <div class=\"article-info\"><span class=\"date\">%s</span><span class=\"tags\">%s</span></div>"
	    entry
            (org-publish-find-title entry project)
	    (org-publish-find-property entry :description project 'html)
	    (format-time-string "%Y-%m-%d" (org-publish-find-date entry project))
	    (org-publish-find-property entry :keywords project 'html)))

(defun lujok/old-sitemap-format-entry (entry style project)
  (format "
    [[file:%s][%s]]
    #+HTML: <div class=\"article-info-old\"><span class=\"date\">%s</span><span class=\"tags\">%s</span></div>
    #+HTML: <div class=\"description\">%s</div>"
	    entry
            (org-publish-find-title entry project)
	    (format-time-string "%Y-%m-%d" (org-publish-find-date entry project))
	    (org-publish-find-property entry :keywords project 'html)
	    (org-publish-find-property entry :description project 'html)))

(defun lujok/note-sitemap-format-entry (entry style project)
  "Format sitemap entry for ENTRY STYLE PROJECT."
  (cond ((not (directory-name-p entry))
         (format "[[file:%s][%s]] [<%s>]"
                 entry
                 (org-publish-find-title entry project)
		 (format-time-string "%Y-%m-%d" (org-publish-find-date entry project))))
        ((eq style 'list)
         ;; Return only last subdir.
         (file-name-nondirectory (directory-file-name entry)))
        (t entry)))

(defun lujok/archive-sitemap-format-entry (entry style project)
  "Format sitemap entry for ENTRY STYLE PROJECT."
  (cond ((not (directory-name-p entry))
         (format "%s » [[file:%s][%s]]"
                 (format-time-string "%Y-%m-%d" (org-publish-find-date entry project))
                 entry
                 (org-publish-find-title entry project)))
        ((eq style 'list)
         ;; Return only last subdir.
         (file-name-nondirectory (directory-file-name entry)))
        (t entry)))

(defun lujok/org-publish-sitemap--valid-entries (entries)
  "Filter ENTRIES that are not valid or skipped by the sitemap entry function."
  (-filter (lambda (x) (car x)) entries))

(defun lujok/latest-posts-sitemap-function (title sitemap)
  "posts.org generation. Only publish the latest 10 posts from SITEMAP (https://orgmode.org/manual/Sitemap.html).  Skips TITLE."
  (let* ((posts (cdr sitemap))
         (posts (lujok/org-publish-sitemap--valid-entries posts))
         (last-five (seq-subseq posts 0 (min (length posts) 10))))
    (org-list-to-org (cons (car sitemap) last-five))))

(defun lujok/format-rss-feed (title list)
  "Generate RSS feed, as a string.
TITLE is the title of the RSS feed.  LIST is an internal
representation for the files to include, as returned by
`org-list-to-lisp'.  PROJECT is the current project."
  (concat "#+TITLE: " title "\n\n"
          (org-list-to-subtree list nil '(:icount "" :istart ""))))

(defun lujok/org-rss-publish-to-rss (plist filename pub-dir)
  "Publish RSS with PLIST, only when FILENAME is 'rss.org'.
PUB-DIR is when the output will be placed."
  (if (equal "rss.org" (file-name-nondirectory filename))
      (org-rss-publish-to-rss plist filename pub-dir)))

(defun lujok/format-rss-feed-entry (entry style project)
  "Format ENTRY for the RSS feed.
ENTRY is a file name.  STYLE is either 'list' or 'tree'.
PROJECT is the current project."
  (cond ((not (directory-name-p entry))
         (let* ((file (org-publish--expand-file-name entry project))
		(title (org-publish-find-title entry project))
		(description (org-publish-find-property entry :description project 'html))
                (date (format-time-string "%Y-%m-%d" (org-publish-find-date entry project)))
                (link (concat (file-name-sans-extension entry) ".html")))
           (with-temp-buffer
             (insert (format "* [[file:%s][%s]]\n" entry title))
             (org-set-property "RSS_PERMALINK" link)
	     (org-set-property "RSS_TITLE" title)
             (org-set-property "PUBDATE" date)
	     (org-set-property "DESCRIPTION" description)
             (insert-file-contents file)
             (buffer-string))))
        ((eq style 'tree)
         ;; Return only last subdir.
         (file-name-nondirectory (directory-file-name entry)))
        (t entry)))

(defun blog-org-html-close-tag (tag &rest attrs)
  "Return close-tag for string TAG.
ATTRS specify additional attributes."
  (concat "<" tag " "
          (mapconcat (lambda (attr)
                       (format "%s=\"%s\"" (car attr) (cadr attr)))
                     attrs
                     " ")
	  ">"))

(defun blog-html-head-extra (file project)
  "Return <meta> elements for nice unfurling on Twitter and Slack."
  (let* ((info (cdr project))
         (org-export-options-alist
          `((:title "TITLE" nil nil parse)
            (:date "DATE" nil nil parse)
            (:author "AUTHOR" nil ,(plist-get info :author) space)
            (:description "DESCRIPTION" nil nil newline)
            (:keywords "KEYWORDS" nil nil space)
            (:meta-image "META_IMAGE" nil ,(plist-get info :meta-image) nil)
            (:meta-type "META_TYPE" nil ,(plist-get info :meta-type) nil)))
         (title (org-publish-find-title file project))
         (date (org-publish-find-date file project))
         (author (org-publish-find-property file :author project))
         (description (org-publish-find-property file :description project))
         (link-home (file-name-as-directory (plist-get info :html-link-home)))
         (extension (or (plist-get info :html-extension) org-html-extension))
	 (rel-file (org-publish-file-relative-name file info))
         (full-url (concat link-home (file-name-sans-extension rel-file) "." extension))
         (image (concat link-home (org-publish-find-property file :meta-image project)))
         (type (org-publish-find-property file :meta-type project)))
    (mapconcat 'identity
               `(,(blog-org-html-close-tag "link" '(rel alternate) '(type application/rss+xml) '(href "https://waychan.cn/blog/rss.xml") '(title "RSS feed"))
                 ,(blog-org-html-close-tag "meta" '(property og:title) `(content ,title))
                 ,(blog-org-html-close-tag "meta" '(property og:url) `(content ,full-url))
                 ,(and description
                       (blog-org-html-close-tag "meta" '(property og:description) `(content ,description)))
                 ,(blog-org-html-close-tag "meta" '(property og:image) `(content ,image))
                 ,(blog-org-html-close-tag "meta" '(property og:type) `(content ,type))
                 ,(blog-org-html-close-tag "meta" '(property twitter:title) `(content ,title))
                 ,(blog-org-html-close-tag "meta" '(property twitter:url) `(content ,full-url))
                 ,(blog-org-html-close-tag "meta" '(property twitter:image) `(content ,image))
                 ,(and description
                       (blog-org-html-close-tag "meta" '(property twitter:description) `(content ,description)))
                 ,(and description
                       (blog-org-html-close-tag "meta" '(property twitter:card) '(content summary)))
                 )
               "\n")))

(defun lujok/org-html-publish-to-html (plist filename pub-dir)
  "Wrapper function to publish an file to html.
PLIST contains the properties, FILENAME the source file and
  PUB-DIR the output directory."
  (let ((project (cons 'wq plist)))
    (plist-put plist :html-head-extra
               (blog-html-head-extra filename project))
    (org-html-publish-to-html plist filename pub-dir)))

;; from https://github.com/aang7/aang7.github.io/blob/master/publish.el
(defun add-class-to-tag (tag class)
  "Add class attribute with the class variable value.
TAG: Tag to modify.
CLASS: Class in string form to add."
;  (interactive "sTag:\nsClass:")
  (setq text-to-search (format "<%s" tag))
  (goto-char (point-min))
  (setq does-it-have-class-attribute t)
  (cl-loop repeat (how-many text-to-search)  do ;; this is like a for loop
       	   (search-forward text-to-search)
	   (setq x-start (point))
	   (setq does-it-have-class-attribute (search-forward
					       "class=\""
					       (line-end-position)
					       t ; if fails return nil
					       ))
	   (if (not does-it-have-class-attribute)
	       (progn ;; then
		 (insert (format " class=\"%s\"" class))
		 (setq does-it-have-class-attribute nil))
	     (progn ;; else
	       (search-forward "\"")
	       (backward-char)
	       (insert (format " %s" class))
	       ))))

(defun add-content-before-tag (tag content)
  "You have to write the exact string of the tag to add before it.
This function only works for html tags, that means that tags has to
 be wrapped with '<' and '>'
TAG: Tag to modify.
CONTENT: string to add."
  ;; (interactive "sTag:\nsContent:")
  (goto-char (point-min)) ; go to the start of the file
  (condition-case nil
      (progn
        (search-forward tag nil t) ;; this always will return nil
	  (search-backward "<" nil t)	  
	  (insert content)	 	
	  (indent-for-tab-command)	 
	  )
    (error nil))
  )

(defun lujok/blog-publish-to-html (plist filename pub-dir)
  "Same as `org-html-publish-to-html' but modifies html before finishing."
  (let ((file-path (lujok/org-html-publish-to-html plist filename pub-dir)))
    (with-current-buffer (find-file-noselect file-path)
      (when (and (string-match "content/blog" filename) (not (string-match "index" filename)))
	(add-content-before-tag "</main" (concat waline)
))      
      (save-buffer)
      (kill-buffer)
      )
    file-path))

(defun lujok/org-html-format-headline-function (todo todo-type priority text tags info)
  "Format a headline with a link to itself.
This function takes six arguments:
TODO      the todo keyword (string or nil).
TODO-TYPE the type of todo (symbol: ‘todo’, ‘done’, nil)
PRIORITY  the priority of the headline (integer or nil)
TEXT      the main headline text (string).
TAGS      the tags (string or nil).
INFO      the export options (plist)."
  (let* ((headline (get-text-property 0 :parent text))
         (id (or (org-element-property :CUSTOM_ID headline)
                 (org-export-get-reference headline info)
                 (org-element-property :ID headline)))
         (link (if id
                   (format "%s <a class=\"headline-ref\" href=\"#%s\">%s</a>" text id "
<svg viewBox=\"0 0 16 16\" version=\"1.1\" width=\"16\" height=\"16\" aria-hidden=\"true\">
<path fill-rule=\"evenodd\" d=\"M7.775 3.275a.75.75 0 001.06 1.06l1.25-1.25a2 2 0 112.83 2.83l-2.5 2.5a2 2 0 01-2.83 0 .75.75 0 00-1.06 1.06 3.5 3.5 0 004.95 0l2.5-2.5a3.5 3.5 0 00-4.95-4.95l-1.25 1.25zm-4.69 9.64a2 2 0 010-2.83l2.5-2.5a2 2 0 012.83 0 .75.75 0 001.06-1.06 3.5 3.5 0 00-4.95 0l-2.5 2.5a3.5 3.5 0 004.95 4.95l1.25-1.25a.75.75 0 00-1.06-1.06l-1.25 1.25a2 2 0 01-2.83 0z\"></path>
</svg>
")
                 text)))
    (org-html-format-headline-default-function todo todo-type priority link tags info)))

;; Set project
(setq org-publish-project-alist
      `(("blog"
	 :base-directory ,(project-dir "content/blog")
	 :base-extension "org"
	 :publishing-directory ,(project-dir "public/blog")
         :publishing-function lujok/blog-publish-to-html
         :exclude ,(regexp-opt '("rss.org"))
	 :recursive t
	 :htmlized-source t
	 :headline-level 4
	 :html-head-include-default-style nil
	 :html-head-include-scripts nil
	 :html-link-home ,website-url
	 :html-home/up-format ""
	 ;:html-format-headline-function lujok/org-html-format-headline-function
	 :html-head ,header
         :html-preamble ,preamble
         :html-postamble ,postamble
	 :auto-sitemap t
	 :sitemap-filename "index.org"
	 :sitemap-title ,blog-title
	 :sitemap-function lujok/blog-index
	 :sitemap-format-entry lujok/sitemap-format-entry
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 )
	("classic"
	 :base-directory ,(project-dir "content/note/classic")
	 :base-extension "org"
	 :exclude ,(regexp-opt '("index.org"))
	 :publishing-directory ,(project-dir "public/note/classic")
         :publishing-function ignore
	 :recursive t
	 :auto-sitemap t
	 :sitemap-filename "index.org"
	 :sitemap-function lujok/note-index
	 :sitemap-format-entry lujok/note-sitemap-format-entry
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 )
	("tcm"
	 :base-directory ,(project-dir "content/note/tcm")
	 :base-extension "org"
	 :exclude ,(regexp-opt '("index.org"))
	 :publishing-directory ,(project-dir "public/note/tcm")
         :publishing-function ignore
	 :recursive t
	 :auto-sitemap t
	 :sitemap-filename "index.org"
	 :sitemap-function lujok/note-index
	 :sitemap-format-entry lujok/note-sitemap-format-entry
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 )
	("taichi"
	 :base-directory ,(project-dir "content/note/taichi")
	 :base-extension "org"
	 :exclude ,(regexp-opt '("index.org"))
	 :publishing-directory ,(project-dir "public/note/taichi")
         :publishing-function ignore
	 :recursive t
	 :auto-sitemap t
	 :sitemap-filename "index.org"
	 :sitemap-function lujok/note-index
	 :sitemap-format-entry lujok/note-sitemap-format-entry
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 )
	("others"
	 :base-directory ,(project-dir "content/note/others")
	 :base-extension "org"
	 :exclude ,(regexp-opt '("index.org"))
	 :publishing-directory ,(project-dir "public/note/others")
         :publishing-function ignore
	 :recursive t
	 :auto-sitemap t
	 :sitemap-filename "index.org"
	 :sitemap-function lujok/note-index
	 :sitemap-format-entry lujok/note-sitemap-format-entry
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 )
	("note"
	 :base-directory ,(project-dir "content/note")
	 :base-extension "org"
	 :publishing-directory ,(project-dir "public/note")
         :publishing-function lujok/blog-publish-to-html
	 :recursive t
	 :htmlized-source t
	 :headline-level 4
	 :html-head-include-default-style nil
	 :html-head-include-scripts nil
	 :html-link-home ,website-url
	 :html-home/up-format ""
	 ;:html-format-headline-function lujok/org-html-format-headline-function
	 :html-head ,header
         :html-preamble ,preamble
         :html-postamble ,postamble
	 :auto-sitemap nil
	 )
	 ("old"
	 :base-directory ,(project-dir "content/old")
	 :base-extension "org"
	 :publishing-directory ,(project-dir "public/old")
         :publishing-function lujok/blog-publish-to-html
	 :recursive t
	 :htmlized-source t
	 :headline-level 4
	 :html-head-include-default-style nil
	 :html-head-include-scripts nil
	 :html-link-home ,website-url
	 :html-home/up-format ""
	 ;:html-format-headline-function lujok/org-html-format-headline-function
	 :html-head ,header
         :html-preamble ,preamble
         :html-postamble ,postamble
	 :auto-sitemap t
	 :sitemap-filename "index.org"
	 :sitemap-title ,old-blog-title
	 :sitemap-function lujok/old-blog-index
	 :sitemap-format-entry lujok/old-sitemap-format-entry
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 )
	("recentpost"
	 :base-directory ,(project-dir "content")
	 :base-extension "org"
	 :exclude ,(regexp-opt '("blog/rss.org" "index.org" "about.org" "404.org" "readings.org" "collections.org" "recentposts.org" "README.org" "work/memo.org"))
	 :publishing-directory ,(project-dir "public")
         :publishing-function ignore
	 :recursive t
	 :auto-sitemap t
	 :sitemap-filename "recentposts.org"
	 :sitemap-title ,website-title
	 :sitemap-function lujok/latest-posts-sitemap-function
	 :sitemap-format-entry lujok/archive-sitemap-format-entry
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 )
	("pages"
	 :base-directory ,(project-dir "content")
	 :base-extension "org"
	 :exclude ,(regexp-opt '("recentposts.org" "README.org"))
	 :publishing-directory ,(project-dir "public")
         :publishing-function lujok/blog-publish-to-html
	 :recursive nil
	 :htmlized-source t
	 :headline-level 4
	 :html-head-include-default-style nil
	 :html-head-include-scripts nil
	 :html-link-home ,website-url
	 :html-home/up-format ""
	 :html-head ,header
         :html-preamble ,preamble
         :html-postamble ,postamble
	 )
        ("assets"
         :base-directory ,(project-dir "assets")
         :base-extension any
         :publishing-directory ,(project-dir "public")
         :publishing-function org-publish-attachment
         :recursive t
	 :exclude ,(regexp-opt '("public" ".*\.org")))
	("images"
         :base-directory ,(project-dir "content/images")
         :base-extension any
         :publishing-directory ,(project-dir "public/images")
         :publishing-function org-publish-attachment
         :recursive t)
        ("blog-rss"
         :base-directory ,(project-dir "content/blog")
         :base-extension "org"
         :recursive t
       	 :exclude ,(regexp-opt '("index.org" "rss.org"))
         :publishing-directory ,(project-dir "public/blog")
         :publishing-function lujok/org-rss-publish-to-rss
         :html-link-home ,blog-url
         :html-link-use-abs-url t
	 :rss-extension "xml"
	 :rss-image-url ,(concat website-url "/images/feed.png")
         :auto-sitemap t
	 :sitemap-filename "rss.org"
	 :sitemap-title ,blog-title
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 :sitemap-function lujok/format-rss-feed
	 :sitemap-format-entry lujok/format-rss-feed-entry
	 )
        ("website" :components ("blog" "note" "old"  "pages" "assets" "images" "blog-rss"))))

(provide 'publish)
